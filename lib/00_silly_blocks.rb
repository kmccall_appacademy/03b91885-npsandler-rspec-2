def reverser(&block)
  arr =  yield.split
  arr.map!(&:reverse)
  arr.join(' ')
end

def adder(el=1, &block)
  yield + el
end

def repeater(times=1, &block)
  i = 1

  while i <= times
    yield
    i += 1
  end
end


# def my_method(&block)
#   puts block
#   block.call
# end
#
# my_method { puts "Hello!" }
