def eval_block(*args, &prc)
  if prc.nil?
    raise "NO BLOCK GIVEN"
  else
    yield(*args)
  end
end


# ```ruby
# # Example calls to eval_block
# eval_block("Kerry", "Washington", 23) do |fname, lname, score|
#   puts "#{lname}, #{fname} won #{score} votes."
# end
# # Washington, Kerry won 23 votes.
# # => nil
#
# eval_block(1,2,3,4,5) do |*args|
#   args.inject(:+)
# end
# # => 15
#
# eval_block(1, 2, 3)
# # => "NO BLOCK GIVEN"
# ```
